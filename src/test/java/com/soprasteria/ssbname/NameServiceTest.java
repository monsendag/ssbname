package com.soprasteria.ssbname;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
class NameServiceTest {

    @Autowired
    private CsvService csvService;

    @Autowired
    private NameService nameService;

    private List<YearStats> stats = new ArrayList<>();

    @BeforeEach
    void setUp() {
        stats = csvService.getBoysStats();
    }

    @Test
    void uniqueNames() {
        long numUniqueNames = nameService.uniqueNames(stats);
        assertEquals(70, numUniqueNames);
    }

    @Test
    void capitalize() {
        List<YearStats> capitalized = nameService.capitalize(stats);

        capitalized.stream().flatMap(yearStats -> yearStats.getNames().stream())
                .forEach(name -> assertTrue(name.matches("^[A-ZÆØÅ][a-zæøå]+$")));
    }

    @Test
    void countPerName() {
        Map<String, Long> countPerName = nameService.countPerName(stats);
        assertEquals(28, countPerName.get("HARALD"));
        assertEquals(1, countPerName.get("ISAK"));
        assertEquals(50, countPerName.get("JAN"));
        assertEquals(36, countPerName.get("KNUT"));
    }

    @Test
    void maxOccurrence() {
        String maxOccurrence = nameService.maxOccurrence(stats);
        assertEquals("KRISTIAN", maxOccurrence);
    }

    @Test
    void leastPopular() {
        String leastPopularName = nameService.leastPopular(stats);
        assertEquals("ISAK", leastPopularName);
    }

    @Test
    void anagrams() {
        List<String> anagrams = nameService.anagrams(stats);
        assertEquals(Arrays.asList("ANDERS", "SANDER"), anagrams);
    }

}