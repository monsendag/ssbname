package com.soprasteria.ssbname;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Service
public class NameService {

    /**
     * Hvor mange unike guttenavn finnes i listen?
     */
    public long uniqueNames(List<YearStats> stats) {
        return stats.stream()
                .flatMap(yearStats -> yearStats.getNames().stream())
                .distinct()
                .count();
    }


    /**
     * Konverter listen til navn med stor forbokstav
     */
    public List<YearStats> capitalize(List<YearStats> stats) {
        return stats.stream().map(yearStats -> {
            List<String> capitalizedNames = yearStats.getNames().stream()
                    .map(String::toLowerCase)
                    .map(StringUtils::capitalize)
                    .collect(toList());
            return new YearStats(yearStats.getYear(), capitalizedNames);
        })
                .collect(toList());
    }

    /**
     * Lag en oversikt over hvor mange ganger de unike guttenavnene går igjen fra 1880 - 2019?
     */
    public Map<String, Long> countPerName(List<YearStats> stats) {
        return stats.stream()
                .flatMap(yearStats -> yearStats.getNames().stream())
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));
    }

    /**
     * Lag en metode som henter ut det guttenavnet som går igjen flest ganger.
     */
    public String maxOccurrence(List<YearStats> stats) {
        Map<String, Long> countPerName = countPerName(stats);

        Optional<Map.Entry<String, Long>> max = countPerName
                .entrySet()
                .stream()
                .reduce((a, b) -> a.getValue() >= b.getValue() ? a : b);

        return max.map(Map.Entry::getKey).orElse(null);
    }


    /**
     * Lag en metode som henter ut det minst populære guttenavnet.
     * <p>
     * Til diskusjon; Hvordan skal vi bedømme hvilket guttenavn som er mest populært?
     */
    public String leastPopular(List<YearStats> stats) {
        Map<String, Integer> scores = new HashMap<>();

        stats.forEach(yearStats -> {
            List<String> names = yearStats.getNames();
            for (int i = 0; i < names.size(); i++) {
                String name = names.get(i);
                int yearScore = 10 - i;
                int currentScore = scores.getOrDefault(name, 0);
                scores.put(name, currentScore + yearScore);
            }
        });

        Optional<Map.Entry<String, Integer>> max = scores
                .entrySet()
                .stream()
                .reduce((a, b) -> a.getValue() < b.getValue() ? a : b);

        return max.map(Map.Entry::getKey).orElse(null);
    }

    /**
     * Hent ut en liste over de guttenavn som er anagrammer
     */
    public List<String> anagrams(List<YearStats> stats) {
        Map<String, Set<String>> anagrams = new HashMap<>();

        stats.stream().flatMap(yearStats -> yearStats.getNames().stream())
                .forEach(name -> {
                    char[] chars = name.toCharArray();
                    Arrays.sort(chars);
                    String sorted = new String(chars);
                    Set<String> nameList = anagrams.getOrDefault(sorted, new HashSet<>());
                    nameList.add(name);
                    anagrams.put(sorted, nameList);
                });

        return anagrams.entrySet().stream()
                .filter(entry -> entry.getValue().size() > 1)
                .flatMap(entry -> entry.getValue().stream())
                .collect(toList());
    }

}
