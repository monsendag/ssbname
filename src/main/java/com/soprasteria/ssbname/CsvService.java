package com.soprasteria.ssbname;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.Reader;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CsvService {

    public List<YearStats> getBoysStats() {
        try {
            return getStats("tabell-gutter.csv");
        } catch (Exception e) {
            throw new IllegalArgumentException("failed to read csv");
        }
    }

    public List<YearStats> getGirlsStats() {
        try {
            return getStats("tabell-jenter.csv");
        } catch (Exception e) {
            throw new IllegalArgumentException("failed to read csv");
        }
    }

    public List<YearStats> getStats(String csvFileName) throws URISyntaxException, IOException, CsvException {
        Reader reader = Files.newBufferedReader(Paths.get(ClassLoader.getSystemResource(csvFileName).toURI()));

        CSVParser parser = new CSVParserBuilder()
                .withSeparator(';')
                .withIgnoreQuotations(true)
                .build();

        CSVReader csvReader = new CSVReaderBuilder(reader)
                .withSkipLines(1)
                .withCSVParser(parser)
                .build();

        List<YearStats> years = csvReader.readAll().stream()
                .peek(strings -> {
                    for (int i = 0; i < strings.length; i++) {
                        strings[i] = strings[i].replace("*", "");
                    }
                })
                .map(YearStats::fromCsvRow)
                .collect(Collectors.toList());

        return years;
    }


}
