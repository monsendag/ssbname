package com.soprasteria.ssbname;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/***
 * A class representing the ten top names of a given year
 */
public class YearStats {

    /**
     * Year representing the top ten boy names
     */
    private final int year;
    private final List<String> names;

    public YearStats(int year, List<String> names) {
        this.year = year;
        this.names = names;
    }

    public static YearStats fromCsvRow(String[] row) {
        int year = Integer.parseInt(row[0]);
        List<String> names = Arrays
                .stream(row)
                .skip(1)
                .filter(StringUtils::isNotBlank)
                .collect(Collectors.toList());
        return new YearStats(year, names);
    }

    public int getYear() {
        return year;
    }

    public List<String> getNames() {
        return names;
    }
}