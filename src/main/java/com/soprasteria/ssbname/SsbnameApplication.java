package com.soprasteria.ssbname;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsbnameApplication {

	public static void main(String[] args) {
		SpringApplication.run(SsbnameApplication.class, args);
	}

}
