# SSB Name 

Intervjucase som dreier seg om SSB navnestatistikk. 

* Implementasjon skal finnes i `NameService.java`
* Tester finnes i `NameServiceTest.java`

### Løsningsforslag

Et løsningsforslag ligger på branchen `solution`  
      ```
      git checkout solution
      ```

### Oppgaver

1. Hvor mange unike guttenavn finnes i listen?


2. Konverter listen til navn med stor forbokstav  

   
3. Lag en oversikt over hvor mange ganger de unike guttenavnene går igjen fra 1880 - 2019?


4. Lag en metode som henter ut det guttenavnet som går igjen flest ganger. 

   
5. Lag en metode som henter ut det minst populære guttenavnet.  
   Diskusjon: Hvordan skal vi bedømme hvilket guttenavn som er mest populært?
   
   
6. Hent ut en liste over de guttenavn som er anagrammer

